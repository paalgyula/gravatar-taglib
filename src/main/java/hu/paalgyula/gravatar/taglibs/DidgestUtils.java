package hu.paalgyula.gravatar.taglibs;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: jethro.gibbs
 * Date: 2012.11.20.
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
class DidgestUtils {
    private static final Logger LOGGER = Logger.getLogger(DidgestUtils.class.getName());

    static String md5Hex(String message) {
        try {
            byte[] hash = MessageDigest.getInstance("MD5").digest(message.getBytes());
            BigInteger bi = new BigInteger(1, hash);
            String result = bi.toString(16);

            // Padding
            if (result.length() % 2 != 0) {
                return "0" + result;
            }

            return result;
        } catch (NoSuchAlgorithmException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
        return null;
    }
}
