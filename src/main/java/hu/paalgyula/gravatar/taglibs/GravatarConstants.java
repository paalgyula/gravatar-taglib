package hu.paalgyula.gravatar.taglibs;

/**
 * Created by paalgyula on 2017.01.08..
 */
public class GravatarConstants {
    public static final String GRAVATAR_HTTP_PREFIX = "http://www.gravatar.com";

    public static final String GRAVATAR_HTTPS_PREFIX = "https://secure.gravatar.com";
}
