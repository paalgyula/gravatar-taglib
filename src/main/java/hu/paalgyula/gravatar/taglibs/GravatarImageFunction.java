package hu.paalgyula.gravatar.taglibs;

import static hu.paalgyula.gravatar.taglibs.DidgestUtils.md5Hex;

/**
 * Created with IntelliJ IDEA.
 * User: paal.gyula
 * Date: 2012.11.20.
 * Time: 14:48
 */
public class GravatarImageFunction {
    /**
     * Generate a link for gravatar
     * @param email - image owner email
     * @param size - image size
     * @param useHttps - plain or https protocol
     * @return
     */
    public static String image( String email, int size, boolean useHttps ) {
        String md5hash = md5Hex( email.trim().toLowerCase() );

        String baseURL = GravatarConstants.GRAVATAR_HTTP_PREFIX;
        if ( useHttps )
            baseURL = GravatarConstants.GRAVATAR_HTTPS_PREFIX;

        return String.format("%s/avatar/%s?size=%d", baseURL, md5hash, size);
    }
}
