package hu.paalgyula.gravatar.taglibs;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.logging.Logger;

import static hu.paalgyula.gravatar.taglibs.DidgestUtils.md5Hex;

/**
 * Created with IntelliJ IDEA.
 * User: paal.gyula
 * Date: 2012.11.19.
 * Time: 20:03
 */
public class GravatarImageTag extends TagSupport {
    private static final Logger LOGGER = Logger.getLogger(GravatarImageTag.class.getName());

    private String email;
    private int size = 32;

    private boolean forceHttps = false;

    public void setEmail(String email) {
        this.email = email;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setForceHttps(boolean forceHttps) {
        this.forceHttps = forceHttps;
    }

    @Override
    public int doStartTag() throws JspException {
        LOGGER.fine("Creating gravatar link for email: " + email);
        String md5hash = md5Hex(email.trim().toLowerCase());

        String baseURL = GravatarConstants.GRAVATAR_HTTP_PREFIX;

        if (forceHttps || pageContext.getRequest().getProtocol().equals("https"))
            baseURL = GravatarConstants.GRAVATAR_HTTPS_PREFIX;

        JspWriter writer = pageContext.getOut();

        try {
            writer.write("<img src=\"");

            String link = String.format("%s/avatar/%s?size=%d", baseURL, md5hash, size);
            writer.write(link);

            writer.write("\" alt=\"\" class=\"gravatar-image\"/>");
        } catch (IOException e) {
            throw new JspException(e);
        }

        return SKIP_BODY;
    }
}
