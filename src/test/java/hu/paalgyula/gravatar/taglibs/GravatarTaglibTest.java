package hu.paalgyula.gravatar.taglibs;

import junit.framework.Assert;
import org.junit.Test;

import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: paalgyula
 * Date: 2012.11.21.
 */
public class GravatarTaglibTest {
    private static final Logger LOGGER = Logger.getLogger(GravatarTaglibTest.class.getName());

    @Test
    public void test_md5() throws Exception {
        String testEmail = "paalgyula@paalgyula.com";
        assertEquals(DidgestUtils.md5Hex( testEmail ), "74420c791a6341d5e4a04031b04b243e");
    }

    @Test
    public void test_image_url() throws Exception {
        String testEmail = "paalgyula@paalgyula.com";
        String gravatarUrl = GravatarImageFunction.image(testEmail, 32, false);

        Assert.assertEquals("http://www.gravatar.com/avatar/74420c791a6341d5e4a04031b04b243e?size=32", gravatarUrl);
    }
}
